#!usr/bin/env python

"""
 @name  XBeeModBus
 @info  The program developed to control communication  between XBee modules and PLC broker.
        It get the payload from XBee modules/MQTT Broker and send it to the other side 
        (MQTT Broker/XBee modules respectively).
        each task is done by specific Thread.
        In the version source routing in xbee modules is applied.
 @ver   1.0
 @note
"""

import sys
import queue
import serial
import threading
import select
import time
import datetime
import functools


# XBee
from digi.xbee.devices import XBeeDevice,RemoteXBeeDevice
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.packets.base import XBeeAPIPacket, XBeePacket, GenericXBeePacket, UnknownXBeePacket
from digi.xbee.packets.aft import ApiFrameType
from digi.xbee.util.utils import hex_to_string

# ModBus
from pymodbus.server.sync import StartTcpServer,ModbusBaseRequestHandler
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

# Logging
import logging
logging.basicConfig(level=logging.DEBUG,
                      format='[%(levelname)s] (%(threadName)-9s) %(message)s',)


def main():
            

 ###############################################
   class XBeeClass(threading.Thread):

      def __init__(self,queueIn,queueOut):
         threading.Thread.__init__(self)
         self.running = 1
         self.queueIn = queueIn
         self.queueOut = queueOut
         
         # Addresses of Routers (XBees), sorted by the node ID of each XBee module
         # 64-bit address of remote XBees: key   --> Node Identification
         #                                 value --> 64-bit address
         self.remoteXbee64 = {"R001":"0013A20040C8E51F",
                              "R002":"0013A20040C2653C"}
         
         # 16-bit address of remote XBees  key   --> Node Identification
         #                                 value --> 16-bit address
         self.remoteXbee16 = {"R001":"",
                              "R002":""}
          
         # 16-bit number of hobs to remote XBees  key   --> Node Identification
         #                                        value --> Route to Destination include 16-bit addresses of hubs
         self.noHobs = {"R001":"",
                        "R002":""}
                             
         # 16-bit address of hobs to remote XBees  key   --> Node Identification
         #                                         value --> Route to Destination include 16-bit addresses of hubs
         self.sourceRoute = {"R001":"",
                             "R002":""}
                             
         self.serialPort = "/dev/ttyAMA0"
         self.buadRate = 9600 
         
         self.funCodeDigit = 2
         self.iDDigit = 4       # Length of XBee ID e.g. R123
         self.mQTTMsgDigit = 2  # Length of msg from mQTT
         self.xBeeMsgDigit = 4  # Length of msg from xBee
         
         self.timeDiscProcess = 40  # Range of time to broadcast ND command for many-to-one process
         self.sleepTime = 5  # Second
         
      
      """
        @name   handleMsg
        @info   The method loops through 64-bit remote addresses to find 
                the key attributed to the address in remoteXbee64 dict
      """
      def handleMsg(self,remoteAdd64):
         
         for key, value in self.remoteXbee64.items():
             if (value == remoteAdd64):
                 return key
                 break
         # if nothing was found    
         return "None"
         
      
      """
        @name   checksum
        @info   The method calculates checksum of the given string
                The description can be found in Xbee website in following address
                
                    https://www.digi.com/resources/documentation/Digidocs/
                    90001500/Default.htm#Tasks/t_calculate_checksum.htm%3FTocPath%
                    3DOperate%2520in%2520API%2520mode%7CAPI%2520frame%2520specifications%7C_____1
      """   
      def checksum(self,string):
         
         sumtemp = 0
         for i in range(0,len(string),2):             
            sumtemp = sumtemp + int(string[i:i+2],16)
             
         sumtemp = sumtemp & 0xFF
         sumtemp = 0xFF - sumtemp
         return str(hex(sumtemp))[2:].upper().zfill(2)

         
      """
        @name   createSourceRouteFrame
        @info   The method creates "Create Source Route" frame based on given remote XBee address
        
                Frame Type = 0x21
                Route Command Options = 0x00
                    
                Source Route Frame Data Structure:
                           0                1-2            3           4           5-12             13-14
                    Start Delimiter | Length of data | Frame Type | Frame ID | 64-bit address | 16-bit address | 
                    
                            15                     16                17 - n             n+1
                    Route Command Options | Number of hobs | 16-bit address of hobs | Checksum
      """   
      def createSourceRouteFrame(self,remoteXbee):
         
         startDelimiter = "7E"
         frameType = "21"
         frameID = "00"
         routeCommandOptions = "00"
                    
         frameData = self.remoteXbee64[remoteXbee] + self.remoteXbee16[remoteXbee] + \
                     routeCommandOptions + str(self.noHobs[remoteXbee]) + self.sourceRoute[remoteXbee]
                     
         data = startDelimiter + str(len(frameData)) + frameData + self.checksum(frameData)
         
         frameDataPacket = UnknownXBeePacket(33,frameData.encode())
         sourcerRouteFrame = frameDataPacket.get_frame_spec_data()
         
         logging.debug("Frame Data" + str(sourcerRouteFrame.decode("utf-8", "ignore")))
         
         return frameDataPacket
         
         
      """
        @name   packetRouteRecordCallback
        @info   The method is a callback method fires when packet recived 
                and if its frame type is Route Record Indicator (0xA1) 
                the rf data stores as route to the destination module
                
                Frame Type = 0xA1
                
                Route Record Indicator Frame Data Structure:
                           0                1-2            3           4           5-12             13-14
                    Start Delimiter | Length of data | Frame Type | Frame ID | 64-bit address | 16-bit address | 
                    
                          15                     16                17- n              n+1
                    Receive Options | Number of Addresses | 16-bit address of hobs | Checksum
      """
      def packetRouteRecordCallback (self,packet):
         
         if not isinstance(packet, XBeeAPIPacket):
            return

         frame_type = packet.get_frame_type()
         
         if frame_type == ApiFrameType.ROUTE_RECORD_INDICATOR:
            print (">>> Source Routing is in Process!")
            data = packet.get_frame_spec_data().hex().upper()
            sourceAdd64 = data[2:18]  #packet.x64bit_source_addr
            print("Address packet " + sourceAdd64)
            nodeID = self.handleMsg(sourceAdd64)
            
            if nodeID != "None":
               self.remoteXbee16[str(nodeID)] = data[18:22]
               self.noHobs[nodeID] = int(data[22:26],16)
               
               """ 
                   Sequence of hobs in route record indicator is opposite of source route frame structure,
                   so the following loop change the sequence of hobs and make it suitable for source route frame
                   ABCD in route record indicator frame ---> DCBA in source route frame
               """
               hobTemp = ""
               for i in range(0,self.noHobs[nodeID]):
                  hobTemp = str(data[26+i*4:26+(i+1)*4]) + hobTemp
               self.sourceRoute[nodeID] = str(hobTemp)
               
               print("Node ID: " + nodeID)
               print("16-bit address " + self.remoteXbee16[str(nodeID)])
               print("number of hub " + str(self.noHobs[nodeID]))
               print("hubs " + self.sourceRoute[nodeID])
               
            else:
               print ("Error 04: There is not module with the given NI (packetRouteRecordCallback)!")
         return
        
      
      """
        @name   dataReceivedCallback
        @info   The method is a callback method fires when data recived 
                and if its frame type is RECEIVE PACKET (0x80) [it is done 
                by the library not in the following method] the extrat different 
                parts of the rf data and rearrange them (as follow) to send to broker
                
                        Function Code + Node ID + Message
      """
      def dataReceivedCallback (self,xbee_message):
      
         xBeePayload = xbee_message.data  # Type = Byte array
                                
         xBeePayloadChr = xBeePayload.decode("utf-8", "ignore")
      
         # Extract different parts from the Xbee msg
         funCode = xBeePayloadChr[0:self.funCodeDigit]
         
         # Get 64 bit address, convert to string [.hex()] and captalizing [.upper()]
         nodeAdd64 = xbee_message.remote_device.get_64bit_addr().address.hex().upper()

         # Find Node ID based on given 64-bit address
         nodeID = self.handleMsg(nodeAdd64)
         
         if nodeID != "None":       
            msg = xBeePayloadChr[self.funCodeDigit:self.funCodeDigit+self.xBeeMsgDigit]
            mqttMsg = funCode + nodeID + str(msg)
                
            print ("Msg : ",mqttMsg)
                
            # Put the msg in queue to send to broker
            queueLock.acquire()
            print("XBee, Put Response in QueueIn")
            self.queueIn.put(str(mqttMsg))
            queueLock.release()
         else:
            print ("Error 04: There is not module with the given NI (dataReceivedCallback)!")
         
         return
         
        		
      def run(self):
      
         count = 0  # Used to send ND command in specific period of time
          
         xBeeDevice = XBeeDevice(self.serialPort, self.buadRate)
                  
         print(">>> Starting XBee ... ")
         try:
             print (">>> Connecting to Serial Port ... ")
             xBeeDevice.open()
         except:
             print ("Error 01: Failed to Connect to Serial Port!")
             
             ErrorMqttMsg = "99"+"R000"+"0101"  # R000 is added to comply with message format
             
             queueLock.acquire()
             print("XBee, Put Error 01 in QueueIn")
             self.queueIn.put(str(ErrorMqttMsg))
             queueLock.release()
             
             raise SystemExit
         
         # Add the callback methods for packet and data 
         xBeeDevice.add_data_received_callback(self.dataReceivedCallback)
         xBeeDevice.add_packet_received_callback(self.packetRouteRecordCallback)
         
        
         # Start discovery the remote nodes before entering the infinite loop
         xBeeNetwork = xBeeDevice.get_network()
         xBeeNetwork.start_discovery_process()
         
         while xBeeNetwork.is_discovery_running():
            time.sleep(0.5)
            
         print (">>> Discovery Process Finished!")
         myEvent.set()
         
         while True:
           
            # The msg came from User (MQTT Broker) and transfer to appropriate 
            # XBee (Routers or End devices)
            queueLock.acquire()
            if not self.queueOut.empty():
               # Get the msg sent by MQTT
               print ("QueueOut is not Empty")
               msg=str(self.queueOut.get())
               msg = msg[2:len(msg)-1]
               
               queueLock.release()
               print("Msg From MQTT Class: " + msg)
               remoteID = msg[self.funCodeDigit:self.funCodeDigit+self.iDDigit]
               xBeeMsg = msg[0:self.funCodeDigit] + msg[6:]
               
               
               """
               try:
                  remoteDevice = xBeeNetwork.get_device_by_64(XBee64BitAddress.from_hex_string(self.remoteXbee64[str(remoteID)]))
               except:
                  print("Error 02: Could not find the remote device")
               
               if remoteDevice is None:
                  
                  # Send the error code if the specified node has not been found
                  ErrorMqttMsg = "99"+remoteID+"0102"
                  queueLock.acquire()
                  print("XBee, Put Error 02 in QueueIn")
                  self.queueIn.put(str(ErrorMqttMsg))
                  queueLock.release()
                  
               else:
                  print("Found the remote device")
                  
                  remoteDevice = RemoteXBeeDevice(xBeeDevice, XBee64BitAddress.from_hex_string(self.remoteXbee64[str(remoteID)]))
                  print("XBee Msg: " + xBeeMsg) 
                  print("Remote ID: " + remoteID)
                  #print("Xbee Address: "+ self.remoteXbee64[str(remoteID)])
               
                  try:
                     xBeeDevice.send_data(remote_device,xBeeMsg)
                     print ("Message Sent Successfully")
                  except:
                     print("Error 03: Error in Sending Data To XBee Module!")
                   
                     ErrorMqttMsg = "99"+remoteID+"0103"
                     queueLock.acquire()
                     print("XBee, Put Error 03 in QueueIn")
                     self.queueIn.put(str(ErrorMqttMsg))
                     queueLock.release()
               """
                     
               remoteDevice = RemoteXBeeDevice(xBeeDevice,XBee64BitAddress.from_hex_string(self.remoteXbee64[str(remoteID)]))
               print("XBee Msg: " + xBeeMsg) 
               print("Remote ID: " + remoteID)
               
               """try:
                  sourceRouteFrame = self.createSourceRouteFrame(remoteID) 
               except:
                  print("Error in creating source route frame!")
                
               """
               try:
                  sourceRouteFramePacket = self.createSourceRouteFrame(remoteID)
                  xBeeDevice.send_packet(sourceRouteFramePacket)
               except:
                   print("Error in Creating Source Route!")
                   
               try:
                  
                  xBeeDevice.send_data(remoteDevice,xBeeMsg)
                  print (">>> Message Sent Successfully!")
                  time.sleep(1)
               except:
                  print("Error 03: Error in Sending Data To XBee Module!")
               
                  ErrorMqttMsg = "99"+remoteID+"0103"
                  queueLock.acquire()
                  print("XBee, Put Error 03 in QueueIn")
                  self.queueIn.put(str(ErrorMqttMsg))
                  queueLock.release()
                   
            else:
                queueLock.release()
                
            time.sleep(self.sleepTime)
            
            # The following lines broadcast ND command to fetch many-to-one and routes to every XBee
            count += 1
            if count == int(self.timeDiscProcess/self.sleepTime):
               count = 0
               #xBeeNetwork = xBeeDevice.get_network()
               xBeeNetwork.start_discovery_process()
                  
            
      def kill(self):
         xBeeDevice.close()
         self.running = 0  
         
         
         ####################################################
   class Modbus_Server(threading.Thread):

      def __init__(self,queueIn,queueOut,context):
         threading.Thread.__init__(self)
         self.running = 1
         
         self.queueIn = queueIn
         self.queueOut = queueOut
         self.context = context
                  
         self.funcCodedigit = 2
         self.countdigit = 2
         self.regAddressdigit = 2
         self.regValuesdigit = 5
         
      """
       This section of the class are responsible to make modbus request based on
       the fucntion code in the recived message from user via MQTT cloud, The waits
       for the response from the PLC/PC (Modbus Response) in case of Reading
       Coil/Register and then sends the Response from PLC/PC to the user via
       MQTT cloud (By Publishing in MQTT Class in run function), and in the
       case of write on Coil/Register print the response if it is OK or and error accured.
      """
		
      def run(self):
         
         print(">>> Starting Modbus Server ... ")
         slaveID = 1

         while True:
                    
            queueLock.acquire()
            if not self.queueIn.empty():
               # Get the msg sent by MQTT
               print ("QueueIn is not Empty")
               msg=str(self.queueIn.get())
               queueLock.release()
               print("Msg From MQTT Class: " + msg)
               
               if (len(msg) < self.funcCodedigit+self.countdigit+self.regAddressdigit):
                  print ("Error in number of received data!!")
                  
               else:

                  # Extarct different parts of the msg
                  funCode = msg[0:self.funcCodedigit]
                  count = msg[self.funcCodedigit:self.funcCodedigit+self.countdigit]
                  regAdd = msg[self.funcCodedigit+self.countdigit:self.funcCodedigit+self.countdigit+self.regAddressdigit]
                
  
                  # If the arbitary process is READING     
                  if ((funCode == "1".zfill(self.funcCodedigit)) | (funCode == "2".zfill(self.funcCodedigit)) \
                  | (funCode == "3".zfill(self.funcCodedigit)) | (funCode == "4".zfill(self.funcCodedigit))):
                     print ("The Massage : ", funCode , count , regAdd)
                     # Values given by PLC are list of Integer
                     values = self.context[slaveID].getValues(int(funCode.lstrip('0')),int(regAdd.lstrip('0')),int(count.lstrip('0')))

                     print ("Values : " , values)
                     if (len(values) > 0):
                        response = str(funCode) + str(count) + str(regAdd) + ''.join(str(x).zfill(self.regValuesdigit) for x in values) #Convert list to str

                     queueLock.acquire()
                     print("ModBus Server, Put Response in QueueOut")
                     self.queueOut.put(str(response))
                     queueLock.release()
                  
                  
                  # If the arbitary process is WRITING
                  elif ((funCode == "5".zfill(self.funcCodedigit)) | (funCode == "15".zfill(self.funcCodedigit)) \
                  | (funCode == "6".zfill(self.funcCodedigit)) | (funCode == "16".zfill(self.funcCodedigit))):
                     if (len(msg) < (int(count) * self.regValuesdigit + self.funcCodedigit+self.countdigit+self.regAddressdigit)):
                        print ("Error in number of data")
                     else:
                        values = []
                        for i in range(int(count)):
                           values.append(msg[self.funcCodedigit+self.countdigit+self.regAddressdigit+i*self.regValuesdigit:
                           self.funcCodedigit+self.countdigit+self.regAddressdigit+(i+1)*self.regValuesdigit])
                           
                        print ("The Massage : ", funCode , count , regAdd , values)
                        
                        # Convert items to integer, because PLC accepts only list of integer
                        try:
                           values = map(int,values)
                        except:
                           print ("Error in number of data")
                           
                        # all the items in the context.setValues are Integer or list of Integer
                        self.context[slaveID].setValues(int(funCode.lstrip('0')),int(regAdd.lstrip('0')),values)
                        print ("Write: " , values)
                        time.sleep(2)
                  
                        ret_values = []
                     
                        # Acknowledgement of updating the register/coil
                        ret_values = self.context[slaveID].getValues(int(funCode.lstrip('0')),int(regAdd.lstrip('0')),int(count.lstrip('0')))
                        print ("Values : " , ret_values)
                        if (len(values) > 0):
                           response = str(funCode) + str(count) + str(regAdd) + ''.join(str(x).zfill(self.regValuesdigit) for x in ret_values) #Convert list to str to append it to string sent to broker
                           print("Response: " + response)
                        queueLock.acquire()
                        print("ModBus Server, Put Response in QueueOut")
                        self.queueOut.put(str(response))
                        queueLock.release()
                  
            else:
                queueLock.release()
            time.sleep(5)
                  
            
      def kill(self):
         self.running = 0
 
   
   
   queueLock = threading.Lock()
   queueIn = queue.Queue(10)    # Data enter in MQTT Class
   queueOut = queue.Queue(10)   # Data send out from MQTT Class
   

   
   myXBeeClass = XBeeClass(queueIn, queueOut)
   myXBeeClass.start()
   
   
   # Set up Modbus library and data block stores data from appointed address 
   # in PLC. It is premised 100 array which equals zero at beginning ([0]*100)
   identity = ModbusDeviceIdentification()
   identity.VendorName  = 'Pymodbus'
   identity.ProductCode = 'PM'
   identity.VendorUrl   = 'http://github.com/bashwork/pymodbus/'
   identity.ProductName = 'Pymodbus Server'
   identity.ModelName   = 'Pymodbus Server'
   identity.MajorMinorRevision = '1.0'
   

   store = ModbusSlaveContext(
      di = ModbusSequentialDataBlock(0, [0]*100),
      co = ModbusSequentialDataBlock(0, [0]*100),
      hr = ModbusSequentialDataBlock(0, [0]*100),
      ir = ModbusSequentialDataBlock(0, [0]*100))
   context = ModbusServerContext(slaves=store, single=True)
   
   print(">>> Running as a Server ... ")
   myServer = Modbus_Server(queueIn, queueOut, context)
   myServer.start()
   
   
   Server_IP = "0.0.0.0"
   Server_Port = 502 
   StartTcpServer(context, identity=identity, address=(Server_IP, Server_Port))
   
   
if __name__ == "__main__":
    main()
